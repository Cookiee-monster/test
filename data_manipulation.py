import pandas as pd
import dateparser

df = pd.read_excel(r"C:\Users\grzegorz.kuprewicz\PycharmProjects\claims\Dane\dane.xls", sheet_name="claims", header=0)
df.info()
df.describe()

df.drop("YEARQTR", axis=1, inplace=True)

df.rename(columns={"AGE*GENDER": "AGE_GENDER"}, inplace=True)

# Korekta dat

for column in ["PLCYDATE", "INITDATE", "CLM_DATE", "BIRTH"]:
    df.loc[df[column].notna(), column] = df.loc[df[column].notna(), column].apply(lambda x: dateparser.parse(x))
    df[column] = pd.to_datetime(df[column]).dt.date

df["BIRTH"] = df["BIRTH"].apply(lambda x: x - pd.DateOffset(years=100) if x > pd.to_datetime("2000-01-01") else x).dt.date

for col in ["BLUEBOOK", "CLM_AMT", "OLDCLAIM", "INCOME", "HOME_VAL"]:
    df[col] = df[col].dropna().str.replace("$", "").astype("int")



df.to_csv(r"C:\Users\grzegorz.kuprewicz\PycharmProjects\claims\Dane\claims_cleared.csv")